var http = require('http');

const SHA256 = require('crypto-js/sha256');

class Block{

  constructor(index, timestamp, data, previousHash = ''){

    this.index = index;
    this.timestamp = new Date();
    this.data = data;
    this.previousHash = previousHash;
    this.hash = this.calculateHash();
    this.nonce = 0;

  }

  calculateHash(){

    return SHA256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.data) + this.nonce).toString();
  }

  mineBlock(difficulty){

    while(this.hash.substring(0, difficulty) !== Array(difficulty + 1).join("0")){

      this.nonce++;
      this.hash = this.calculateHash();
    }

    console.log("Block Mined : " + this.hash);

  }
}


class Blockchain{

  constructor(){

    //start the chain with the genesis block
    this.chain = [this.createGenesisBlock()];
    this.difficulty = 1;

  }

  createGenesisBlock(){

    //return the genesis block, the first one
    return new Block(0,"01/01/2018","Sweetpain - interno holocausto","0");

  }

  getLastBlock(){

    return this.chain[this.chain.length -1 ];
  }

  addBlock(newBlock){

    newBlock.previousHash = this.getLastBlock().hash;
    newBlock.mineBlock(this.difficulty);
    this.chain.push(newBlock);

  }

  isChainValid(){

    for(let i = 1; i < this.chain.length; i++){

      const currentBlock = this.chain[i];
      const previousBlock = this.chain[i - 1];

      if(currentBlock.hash !== currentBlock.calculateHash()){

        return false;

      }

      if(currentBlock.previousHash !== previousBlock.hash){

        return false;

      }
    }

    return true;
  }
}



http.createServer(function (req, res) {

    //creating the new object
    let smartVoting = new Blockchain();

    //Creating some blocks to show how the blockchain works :)

    smartVoting.addBlock(new Block(1,this.timestamp,{ voter: "X928373B", candidate : 0 }));
    smartVoting.addBlock(new Block(1,this.timestamp,{ voter: "9851928A", candidate : 2 }));
    smartVoting.addBlock(new Block(1,this.timestamp,{ voter: "X981627W", candidate : 1 }));
    smartVoting.addBlock(new Block(1,this.timestamp,{ voter: "W73926BB", candidate : 2 }));
    smartVoting.addBlock(new Block(1,this.timestamp,{ voter: "Y725141X", candidate : 0 }));


    res.writeHead(200, {'Content-Type': 'text/text'});

    //show the result in JSON format
    res.end(JSON.stringify(smartVoting, null, 4));

}).listen(8080);
